from fastapi import FastAPI

app = FastAPI()

@app.get('/')
async def root():
    return {'msg': 'Hello FastAPI'}

@app.get('/hello')
async def hello():
    return {'msg': "what's up FastAPI"}

@app.post('/testsend')
async def send(data1: int,data2: int):
    print(data1,data2,data1-data2)
    return {'msg': f"ans {data1-data2}"}

# @app.post('/testsend')
# async def send(data: int):
#     print(data)
#     return {'msg': f"send {data}"}
